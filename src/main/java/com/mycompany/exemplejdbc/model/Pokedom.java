
package com.mycompany.exemplejdbc.model;

/**
 *
 * @author mfontana
 */
public class Pokedom {
    
    private String nom;
    private String familia;
    private int atac;
    private int defensa;

    public Pokedom(String nom, String familia, int atac, int defensa) {
        this.nom = nom;
        this.familia = familia;
        this.atac = atac;
        this.defensa = defensa;
    }

    public String getNom() {
        return nom;
    }

    public String getFamilia() {
        return familia;
    }

    public int getAtac() {
        return atac;
    }

    public int getDefensa() {
        return defensa;
    }
    
    
    
}
