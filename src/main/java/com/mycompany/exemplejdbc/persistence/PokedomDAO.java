package com.mycompany.exemplejdbc.persistence;

import com.mycompany.exemplejdbc.model.Pokedom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


/**
 *
 * @author mfontana
 */
public class PokedomDAO {

    private Connection conexio;
    
    public ArrayList<Pokedom> selectAllPokedom() throws SQLException {
        String query = "select * from pokedom;";
        ArrayList<Pokedom> pokedoms = new ArrayList<>();
        Statement st = conexio.createStatement();
        // Executem la consulta i recollim el resultat
        ResultSet rs = st.executeQuery(query);
        while (rs.next()) {
            String nom = rs.getString("nom");
            String familia = rs.getString("familia");
            int atac = rs.getInt("atac");
            int defensa = rs.getInt("defensa");
            pokedoms.add(new Pokedom(nom, familia, atac,defensa));
        }
        rs.close();
        st.close();
        return pokedoms;
    }
    
    public void insertPokedom(Pokedom p) throws SQLException {
        // Pendiente verificar primero que existe
        String insert = "insert into pokedom (nom, familia, atac, defensa) "
                + "values (?, ?, ?,?);";
        PreparedStatement ps = conexio.prepareStatement(insert);
        ps.setString(1, p.getNom());
        ps.setString(2, p.getFamilia());
        ps.setInt(3, p.getAtac());
        ps.setInt(4, p.getDefensa());
        ps.executeUpdate();
        ps.close();
    }
    
    private boolean existePokedom(Pokedom p) {
        // código que verifica si existe
        return false;
    }
           

    public void conectar() throws SQLException, ClassNotFoundException {
        // Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/damlandia";
        String user = "root";
        String pass = "root";
        conexio = DriverManager.getConnection(url, user, pass);
    }
    
    public void desconectar() throws SQLException {
        if (conexio != null) {
            conexio.close();
        }
    }
    

}
