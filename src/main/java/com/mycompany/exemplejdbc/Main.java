
package com.mycompany.exemplejdbc;

import com.mycompany.exemplejdbc.model.Pokedom;
import com.mycompany.exemplejdbc.persistence.PokedomDAO;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mfontana
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            PokedomDAO bbdd = new PokedomDAO();
            bbdd.conectar();
            System.out.println("Conectado a la BBDD");
            Pokedom p = new Pokedom("Pikatxu", "Electrico", 20, 10);
            bbdd.insertPokedom(p);
            System.out.println("Pokedom insertado.");
            bbdd.desconectar();
            System.out.println("Conexión con la BBDD cerrada");
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
}
